import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Game to learn english words. After choosing subject and level you have to properly
 * translate all displayed polish words
 */

public class Game {

    /**
     * prints all available subjects to learn
     */

    public void printSubjects() {
        for (Subject s : Subject.values()) {
            System.out.println(s);
        }
    }

    /**
     * enables to choose one of the subject
     *
     * @param number of subject
     * @return Subject subject
     */

    public Subject chooseSubject(int number) {
        for (Subject s : Subject.values()) {
            if (number == s.getNumber()) {
                return s;
            }
        }
        return null;
    }

    /**
     * enables to choose level
     *
     * @param level (as a number)
     * @return level number
     */

    public int chooseLevel(int level) {
        for (int i = 1; i < 3; i++) {
            if (level == i) {
                return i;
            }
        }
        return 0;
    }

    /**
     * creates list of words of all subjects assigned to level 1 - words are taken from file
     *
     * @return ArraysList
     */

    public List<Word> createWordsLevel1() {
        List<Word> wordsLevel1 = new ArrayList<>();

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("wordsLevel1.csv"));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(" ");
                Word w = new Word(Subject.valueOf(data[0]), data[1], data[2], Integer.parseInt(data[3]));
                wordsLevel1.add(w);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return wordsLevel1;
    }

    /**
     * creates list of words of all subjects assigned to level 2 - words are taken from file
     *
     * @return Array List
     */

    public List<Word> createWordsLevel2() {
        List<Word> wordsLevel2 = new ArrayList<>();

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("wordsLevel2.csv"));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(" ");
                Word w = new Word(Subject.valueOf(data[0]), data[1], data[2], Integer.parseInt(data[3]));
                wordsLevel2.add(w);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return wordsLevel2;
    }

    /**
     * veryfies if user correctly translate polish words from chosen subject and level
     * @param words (Array List)
     */

    public void translate(List<Word> words) {
        Scanner sc = new Scanner(System.in);

        Random random = new Random();

        while (words.size() > 0) {
            int randomIndex = random.nextInt(words.size());

            Word word = words.get(randomIndex);

            System.out.println("Przetłumacz: " + word.getPolishName());
            String translation = sc.nextLine();
            if (translation.equalsIgnoreCase(word.getEnglishName())) {
                System.out.println("Dobrze!");
                words.remove(word);
            } else {
                System.out.printf("Źle, (to: '%s')%n", word.getEnglishName());
            }
        }
    }

    /**
     * retriever the number of subject and level from the user, creates the list of words of chosen subject and level.
     * calls the methid translate and after its properly ends print information of the end of game and return 0;
     *
     * @param level
     * @param subject
     * @return 0 (represents the end of game)
     */


    public int play(int level, int subject) {
        int chosenLevel = chooseLevel(level);
        Subject chosenSubject = chooseSubject(subject);
        List<Word> words = null;

        if (chosenLevel == 1) {
            words = createWordsLevel1();
        }

        if (chosenLevel == 2) {
            words = createWordsLevel2();
        }

        List<Word> filteredWords = words.stream()
                .filter(word -> word.getSubject() == chosenSubject)
                .collect(Collectors.toList());

        translate(filteredWords);

        System.out.println("Zakończono " + level);

        return 0;
    }

}