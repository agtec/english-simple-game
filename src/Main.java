import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        boolean displayMenu = true;
        Game game = new Game();

        do {
            System.out.println("\n\n          Nauka słowek po angielsku");
            System.out.println("--------------------------------------");
            System.out.println("1 - Zagraj");
            System.out.println("2 - Wyjście z gry");
            try {
                System.out.println("Wybierz opcję: (1/2): ");
                int input = sc.nextInt();
                switch (input) {
                    case 1:
                        boolean continueGame = true;
                        do {
                            System.out.println("Wybierz temat z listy: ");
                            game.printSubjects();
                            int subject = sc.nextInt();
                            sc.nextLine();
                            System.out.println("Wpisz poziom: 1 lub 2");
                            int level = sc.nextInt();
                            sc.nextLine();
                            int isGameFinished = game.play(level, subject);
                            System.out.println("Gra zakończono, czy chcesz zagrać jeszcze raz? (t/n)");
                            String answer = sc.nextLine();

                            if (answer.equals("n")) {
                                continueGame = false;
                                break;
                            }

                        } while (continueGame);
                    case 2:
                        System.out.println("Koniec");
                        displayMenu = false;
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (displayMenu);
    }
}