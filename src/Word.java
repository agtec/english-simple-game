public class Word {

    private Enum subject;
    private String polishName;
    private String englishName;
    private int level;

    public Word(Enum subject, String polishName, String englishName, int level) {
        this.subject = subject;
        this.polishName = polishName;
        this.englishName = englishName;
        this.level = level;
    }

    public Enum getSubject() {
        return subject;
    }

    public String getPolishName() {
        return polishName;
    }


    public void setPolishName(String polishName) {
        this.polishName = polishName;
    }

    public String getEnglishName() {
        return englishName;
    }


    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public void setSubject(Enum subject) {
        this.subject = subject;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return getPolishName() + " " + getEnglishName() + " " + getSubject();
    }
}
