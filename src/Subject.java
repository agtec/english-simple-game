public enum Subject {

    HUMAN (1, "Człowiek"),
    ANIMALS(2, "Zwierzęta"),
    BUILDINGS(3, "Budowle"),
    VEHICLES(4, "Pojazdy"),
    TOYS(5, "Zabawki");

    private int number;
    private String translation;

    Subject(int number, String translation) {
        this.number = number;
        this.translation = translation;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public String toString() {
        return getNumber() + " " + super.toString() + ": " + getTranslation();
    }
}
